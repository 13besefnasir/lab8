#include <stdlib.h>
#include <random>
#include <ctime>
#include "matrix.h"
#include "vector.h"
#include "functions.h"
Vector multiply(Matrix a, Vector b){
	int m = a.size_m;
	int n = b.size_v;
	Vector result(m / n);
	int sum = 0;
	int j = 0;
	for (int i = 0; i < m + 1; i++){
		if (i != m){
			sum = sum + (b.vect[i % n] * a.matr[i]);
		}
		if ((i + 1) % n == 0 && i != 0){
			j++;
			result.vect[j] = sum;
			sum = 0;

		}
	}
	return result;
}
