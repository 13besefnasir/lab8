prog1: main.o matrix.o vector.o
	gcc main.o matrix.o vector.o -pthreads prog1
main.o: main.cpp functions.h
	gcc -c main.cpp

matrix.o: matrix.cpp functions.h
	gcc -c factorial.cpp

vector.o: vector.cpp functions.h
	gcc -c hello.cpp
clean:
	rm main.o
